﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.Networking;

public class navigator : MonoBehaviour
{

    private GameObject sideBarGO;
    private GameObject saveLoadSystemGO;
    private GameObject panelMenuGO;
    private GameObject panelMenuPembelajaranGO;
    private GameObject panelPembelajaran1GO;
    private GameObject panelPembelajaran2GO;
    private GameObject panelPembelajaran3GO;
    private GameObject panelPembelajaran4GO;

    private GameObject panelTujuan1GO;
    private GameObject panelTujuan2GO;
    private GameObject panelTujuan3GO;
    private GameObject panelTujuan4GO;

    private GameObject scrollMateri;
    private GameObject materi1;

    saveloadSystem SaveloadSystem;

    private GameObject usernameGO;

    private float moveTimerOpen = 10;
    private float moveTimerClose = 10;
    private float currentSideBarPos;
    private float closedPosition;

    private int selectedMateri;

    private int index;
    private int pdvalue;

    private GameObject scoreText;
    private int scoreInt;

    private string urlDB = "http://ahobaka.my.id/index.php/Welcome/";
    //private string urlDB = "https://emhidrosfer.000webhostapp.com/index.php/Welcome/";

    private void Start()
    {
        sideBarGO = GameObject.Find("sideCanvas");
        saveLoadSystemGO = GameObject.Find("saveLoadSystem");
        usernameGO = GameObject.Find("namaUser");
        panelMenuGO = GameObject.Find("panelMenu");
        panelMenuPembelajaranGO = GameObject.Find("panelMenuPembelajaran");

        panelPembelajaran1GO = GameObject.Find("panelPembelajaran1");
        panelPembelajaran2GO = GameObject.Find("panelPembelajaran2");
        panelPembelajaran3GO = GameObject.Find("panelPembelajaran3");
        panelPembelajaran4GO = GameObject.Find("panelPembelajaran4");

        panelTujuan1GO = GameObject.Find("panelTujuan1");
        panelTujuan2GO = GameObject.Find("panelTujuan2");
        panelTujuan3GO = GameObject.Find("panelTujuan3");
        panelTujuan4GO = GameObject.Find("panelTujuan4");

        scrollMateri = GameObject.Find("scrollMateri");
        materi1 = GameObject.Find("judul2");

        try {
            panelMenuPembelajaranGO.SetActive(false);
            panelPembelajaran1GO.SetActive(false);
            panelPembelajaran2GO.SetActive(false);
            panelPembelajaran3GO.SetActive(false);
            panelPembelajaran4GO.SetActive(false);
            panelTujuan1GO.SetActive(false);
            panelTujuan2GO.SetActive(false);
            panelTujuan3GO.SetActive(false);
            panelTujuan4GO.SetActive(false);
        }
        catch (Exception e) {
            Debug.Log(e);
        }

        try {
            scoreText = GameObject.Find("scoreText");
            scoreText.GetComponent<Text>().text = SaveloadSystem.countPD.ToString();
            scoreInt = int.Parse(scoreText.GetComponent<Text>().text);
        } catch (Exception e) {

        }

        SaveloadSystem = saveLoadSystemGO.GetComponent<saveloadSystem>();

        try {
            usernameGO.GetComponent<Text>().text = SaveloadSystem.nama;
        } catch (Exception e) {
            Debug.Log(e);

        }


    }

    private void Update()
    {

    }


    public void pembelajaran1(int pilihanMateri)
    {
        panelMenuGO.SetActive(false);
        panelMenuPembelajaranGO.SetActive(true);
        selectedMateri = pilihanMateri;
    }

    public void openTujuan() {
        panelMenuPembelajaranGO.SetActive(false);

        if (selectedMateri == 1)
        {
            panelTujuan1GO.SetActive(true);
        }
        else if (selectedMateri == 2)
        {
            panelTujuan2GO.SetActive(true);
        }
        else if (selectedMateri == 3)
        {
            panelTujuan3GO.SetActive(true);
        }
        else if (selectedMateri == 4)
        {
            panelTujuan4GO.SetActive(true);
        }


    }

    public void openMateri() {
        panelMenuPembelajaranGO.SetActive(false);

        if (selectedMateri == 1) {
            panelPembelajaran1GO.SetActive(true);
        }
        else if (selectedMateri == 2) {
            panelPembelajaran2GO.SetActive(true);
        }
        else if (selectedMateri == 3) {
            panelPembelajaran3GO.SetActive(true);
        }
        else if (selectedMateri == 4) {
            panelPembelajaran4GO.SetActive(true);
        }

        //materi(selectedMateri);
    }

    public void materi(int materi) {


    }

    public void materiScroll() {
        materi1.transform.localPosition = new Vector2(0, -540);
        Debug.Log(scrollMateri.GetComponent<Scrollbar>().value);
    }

    public void kompetensi()
    {
        SceneManager.LoadScene("kompetensi");
    }

    public void pembelajaran()
    {
        SceneManager.LoadScene("pembelajaran");
    }

    public void evaluasi()
    {
        SceneManager.LoadScene("evaluasi");
    }

    public void penilaiandiri()
    {
        SceneManager.LoadScene("penilaian");
    }

    public void glosarium()
    {
        SceneManager.LoadScene("glosarium");
    }

    public void backHome()
    {
        SceneManager.LoadScene("dashboard");
    }

    public void logout()
    {
        Destroy(saveLoadSystemGO);
        SceneManager.LoadScene("loginScene");
    }


    public void updatePDIndex(int index) {
        Debug.Log("index change" + index);
        this.index = index - 1;
    }

    public void updatePD(string value)
    {
        Debug.Log("value change " + value);

        Debug.Log("pd value current = " + SaveloadSystem.pd[index]);

        SaveloadSystem.pd[index] = value;



        StartCoroutine(updatePDServer());
    }

    IEnumerator updatePDServer()
    {
        String pdString = string.Join(".", SaveloadSystem.pd); ;
        Debug.Log(pdString);

        //int countPD = pdString.Split('0').Length - 1;
        int countPD = pdString.Replace(".", "").Split('0').Length - 1;
        //countPD = 10 - countPD;

        Debug.Log("count pd in the change = " + countPD);
        scoreText.GetComponent<Text>().text = countPD.ToString();

        UnityWebRequest www = UnityWebRequest.Get(urlDB + "updatePD/" + SaveloadSystem.absen + "/" + pdString);
        Debug.Log(urlDB + "updatePD/" + SaveloadSystem.absen + "/" + pdString);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("update berhasel");

        }
    }


    public void openSideBar() {
        Debug.Log("open side Bar :" + sideBarGO.transform.position.x);
        closedPosition = sideBarGO.transform.position.x;
        //currentSideBarPos = sideBarGO.transform.position.x;
        sideBarGO.transform.position = new Vector2(0, 0);
        moveTimerOpen = 0;


    }

    public void closeSideBar()
    {
        //closedPosition = sideBarGO.transform.position.x;
        currentSideBarPos = sideBarGO.transform.position.x;
        sideBarGO.transform.position = new Vector2(closedPosition, 0);
        moveTimerClose = 0;
    }

    public void openAktifitas1()
    {

        SaveloadSystem.aktifitas = 1;
        SceneManager.LoadScene("aktifitas1");
        
    }

    public void openAktifitas2()
    {
        SaveloadSystem.aktifitas = 2;
        SceneManager.LoadScene("aktifitas1");
    }

    public void openAktifitas3()
    {
        SaveloadSystem.aktifitas = 3;
        SceneManager.LoadScene("aktifitas1");
    }

    public void openAktifitas4()
    {
        SaveloadSystem.aktifitas = 4;
        SceneManager.LoadScene("aktifitas1");
    }
}
