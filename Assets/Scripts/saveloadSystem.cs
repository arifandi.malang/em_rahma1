﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

    public class saveloadSystem : MonoBehaviour
    {
        private GameObject inputNamaGO;
        private GameObject inputAbsenGO;
        private GameObject canvasConfirmationGO;
        private GameObject errorConfirmationGO;
        private GameObject tryConfirmationGO;
        private GameObject errorTextGO;

        private GameObject coverLoadingGO;

        public string id;
        public string nama;
        public string absen;
        public string eval;
        public string[] pd;
        public string[] ans;
        public int score;

        public string ak11;
        public string ak12;
        public string ak13;
        public string ak14;
        public string ak15;

        public string ak21;
        public string ak22;
        public string ak23;
        public string ak24;
        public string ak25;

        public string ak31;
        public string ak32;
        public string ak33;
        public string ak34;
        public string ak35;

        public string ak41;
        public string ak42;
        public string ak43;
        public string ak44;
        public string ak45;

        public int aktifitas;

        public int countPD;
        public int countans;
    
        private string login = "nodata";

        private string urlDB = "http://ahobaka.my.id/index.php/Welcome/";
       // private string urlDB = "https://emhidrosfer.000webhostapp.com/index.php/Welcome/";
 
    void Start()
        {
            inputNamaGO = GameObject.Find("namaInput");
            inputAbsenGO = GameObject.Find("absenInput");
            canvasConfirmationGO = GameObject.Find("canvasConfirmation");
            errorConfirmationGO = GameObject.Find("errorConfirmation");
            tryConfirmationGO = GameObject.Find("tryConfirmation");
            errorTextGO = GameObject.Find("errorText");
            
            canvasConfirmationGO.SetActive(false);
            errorConfirmationGO.SetActive(false);
            tryConfirmationGO.SetActive(false);
            DontDestroyOnLoad(this.gameObject);
        }

        public void checkUser() {
        
            nama = inputNamaGO.GetComponent<InputField>().text;
            absen = inputAbsenGO.GetComponent<InputField>().text;

            if (nama == "" || absen == "") {
            //Debug.Log("full");
                errorConfirmationGO.SetActive(true);
                errorTextGO.GetComponent<Text>().text = "nama & absen tidak boleh kosong";
            }
            else {
                tryConfirmationGO.SetActive(true);
                StartCoroutine(GetUser());
                
            }
            
        }

        public void mendaftar() {
            StartCoroutine(AddUser());
            StartCoroutine(GetUser());
            
        //SceneManager.LoadScene("dashboard");
    }

        public void tidakMendaftar() {
            canvasConfirmationGO.SetActive(false);
        }

        public void retryLogin() {
            errorConfirmationGO.SetActive(false);
        }


        IEnumerator GetUser()
        {
            UnityWebRequest www = UnityWebRequest.Get(urlDB+"cekuser/"+nama+"/"+absen);
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                tryConfirmationGO.SetActive(false);
                errorConfirmationGO.SetActive(true);
                errorTextGO.GetComponent<Text>().text = www.error;
            }
            else
            {
                login = www.downloadHandler.text;

                if (login != "nouser")
                {
                //Debug.Log("user terdaftar");
                    string[] chunks = login.Split('~');
                    id = chunks[0];
                    nama = chunks[1];
                    absen = chunks[2];
                    eval = chunks[3];
                    pd = chunks[4].Split('.');
                    countPD = chunks[4].Replace(".","").Split('0').Length - 1;
                    Debug.Log("this is count PD"+countPD);
                    //countPD = 10 - countPD;
                    ans = chunks[5].Split('.');
                    score = int.Parse(chunks[6]);

                    Debug.Log("id " + id + " nama " + nama+ " absen "+absen+ "PD "+pd[1]+" ANS " + ans[1]);
                    StartCoroutine(GetKeg());
                    
                }
                else
                {
                    Debug.Log("user belum terdaftar");
                    tryConfirmationGO.SetActive(false);
                    canvasConfirmationGO.SetActive(true);
                }

            }
        }

    IEnumerator GetKeg()
    {
        UnityWebRequest www = UnityWebRequest.Get(urlDB + "getPostedText/" + id);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            tryConfirmationGO.SetActive(false);
            errorConfirmationGO.SetActive(true);
            errorTextGO.GetComponent<Text>().text = www.error;
        }
        else
        {
            login = www.downloadHandler.text;

            if (login != "notext")
            {
                //Debug.Log("user terdaftar");
                string[] chunks = login.Split('~');

                ak11 = chunks[1];
                ak12 = chunks[2];
                ak13 = chunks[3];
                ak14 = chunks[4];
                ak15 = chunks[5];

                ak21 = chunks[6];
                ak22 = chunks[7];
                ak23 = chunks[8];
                ak24 = chunks[9];
                ak25 = chunks[10];

                ak31 = chunks[11];
                ak32 = chunks[12];
                ak33 = chunks[13];
                ak34 = chunks[14];
                ak35 = chunks[15];

                ak41 = chunks[16];
                ak42 = chunks[17];
                ak43 = chunks[18];
                ak44 = chunks[19];
                ak45 = chunks[20];

                SceneManager.LoadScene("dashboard");
            }
            else
            {
                Debug.Log("user belum terdaftar");
                tryConfirmationGO.SetActive(false);
                canvasConfirmationGO.SetActive(true);
            }

        }
    }

    IEnumerator AddUser()
    {
        UnityWebRequest www = UnityWebRequest.Get(urlDB + "adduser/" + nama + "/" + absen);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("daftar berhasel");

        }
    }

    


  
}