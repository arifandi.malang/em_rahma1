﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class aktifitas : MonoBehaviour
{

    private GameObject contentParent;
    private GameObject panel1;
    private GameObject panel2;
    private GameObject panel3;
    private GameObject panel4;

    private GameObject ak11;
    private GameObject ak12;
    private GameObject ak13;
    private GameObject ak14;
    private GameObject ak15;
    private GameObject ak21;
    private GameObject ak22;
    private GameObject ak23;
    private GameObject ak24;
    private GameObject ak25;
    private GameObject ak31;
    private GameObject ak32;
    private GameObject ak33;
    private GameObject ak34;
    private GameObject ak35;
    private GameObject ak41;
    private GameObject ak42;
    private GameObject ak43;
    private GameObject ak44;
    private GameObject ak45;

    private float timerCover;
    private GameObject textCover;

    private GameObject imageCover;

    private GameObject saveLoadSystemGO;
    saveloadSystem SaveloadSystem;

    // Start is called before the first frame update
    void Start()
    {
        saveLoadSystemGO = GameObject.Find("saveLoadSystem");
        SaveloadSystem = saveLoadSystemGO.GetComponent<saveloadSystem>();

        timerCover = 1;

        panel1 = GameObject.Find("panelPembelajaran1");
        panel2 = GameObject.Find("panelPembelajaran2");
        panel3 = GameObject.Find("panelPembelajaran3");
        panel4 = GameObject.Find("panelPembelajaran4");

        ak11 = GameObject.Find("ak11");
        ak12 = GameObject.Find("ak12");
        ak13 = GameObject.Find("ak13");
        ak14 = GameObject.Find("ak14");
        ak15 = GameObject.Find("ak15");

        ak21 = GameObject.Find("ak21");
        ak22 = GameObject.Find("ak22");
        ak23 = GameObject.Find("ak23");
        ak24 = GameObject.Find("ak24");
        ak25 = GameObject.Find("ak25");

        ak31 = GameObject.Find("ak31");
        ak32 = GameObject.Find("ak32");
        ak33 = GameObject.Find("ak33");
        ak34 = GameObject.Find("ak34");
        ak35 = GameObject.Find("ak35");

        ak41 = GameObject.Find("ak41");
        ak42 = GameObject.Find("ak42");
        ak43 = GameObject.Find("ak43");
        ak44 = GameObject.Find("ak44");
        ak45 = GameObject.Find("ak45");

        imageCover = GameObject.Find("imageCover");
        textCover = GameObject.Find("textCover");

        panel1.SetActive(false);
        panel2.SetActive(false);
        panel3.SetActive(false);
        panel4.SetActive(false);

        switch (SaveloadSystem.aktifitas) {
            case 1:
                panel1.SetActive(true);
                ak11.GetComponent<InputField>().text = SaveloadSystem.ak11;
                ak12.GetComponent<InputField>().text = SaveloadSystem.ak12;
                ak13.GetComponent<InputField>().text = SaveloadSystem.ak13;
                ak14.GetComponent<InputField>().text = SaveloadSystem.ak14;
                ak15.GetComponent<InputField>().text = SaveloadSystem.ak15;
                
                break;
            case 2:
                panel2.SetActive(true);
                ak21.GetComponent<InputField>().text = SaveloadSystem.ak21;
                ak22.GetComponent<InputField>().text = SaveloadSystem.ak22;
                ak23.GetComponent<InputField>().text = SaveloadSystem.ak23;
                ak24.GetComponent<InputField>().text = SaveloadSystem.ak24;
                ak25.GetComponent<InputField>().text = SaveloadSystem.ak25;
                
                break;
            case 3:
                panel3.SetActive(true);
                ak31.GetComponent<InputField>().text = SaveloadSystem.ak31;
                ak32.GetComponent<InputField>().text = SaveloadSystem.ak32;
                ak33.GetComponent<InputField>().text = SaveloadSystem.ak33;
                ak34.GetComponent<InputField>().text = SaveloadSystem.ak34;
                ak35.GetComponent<InputField>().text = SaveloadSystem.ak35;
                
                break;
            case 4:
                panel4.SetActive(true);
                ak41.GetComponent<InputField>().text = SaveloadSystem.ak41;
                ak42.GetComponent<InputField>().text = SaveloadSystem.ak42;
                ak43.GetComponent<InputField>().text = SaveloadSystem.ak43;
                ak44.GetComponent<InputField>().text = SaveloadSystem.ak44;
                ak45.GetComponent<InputField>().text = SaveloadSystem.ak45;
                break;
        }

    }

    // Update is called once per frame

    private void Update()
    {
        timerCover -= Time.deltaTime;

        if (timerCover <= 0)
        {
            textCover.GetComponent<Text>().text = "Now Loading...";
            imageCover.SetActive(false);
            
        }
        else if (timerCover > 0) {
            imageCover.SetActive(true);
        }
    }

    public void submitButton() {

        timerCover = 1000;

        switch (SaveloadSystem.aktifitas)
        {
            case 1:
                panel1.SetActive(true);
                SaveloadSystem.ak11 = ak11.GetComponent<InputField>().text;
                SaveloadSystem.ak12 = ak12.GetComponent<InputField>().text;
                SaveloadSystem.ak13 = ak13.GetComponent<InputField>().text;
                SaveloadSystem.ak14 = ak14.GetComponent<InputField>().text;
                SaveloadSystem.ak15 = ak15.GetComponent<InputField>().text;

                break;
            case 2:
                panel2.SetActive(true);
                SaveloadSystem.ak21 = ak21.GetComponent<InputField>().text;
                SaveloadSystem.ak22 = ak22.GetComponent<InputField>().text;
                SaveloadSystem.ak23 = ak23.GetComponent<InputField>().text;
                SaveloadSystem.ak24 = ak24.GetComponent<InputField>().text;
                SaveloadSystem.ak25 = ak25.GetComponent<InputField>().text;

                break;
            case 3:
                panel3.SetActive(true);
                SaveloadSystem.ak31 = ak31.GetComponent<InputField>().text;
                SaveloadSystem.ak32 = ak32.GetComponent<InputField>().text;
                SaveloadSystem.ak33 = ak33.GetComponent<InputField>().text;
                SaveloadSystem.ak34 = ak34.GetComponent<InputField>().text;
                SaveloadSystem.ak35 = ak35.GetComponent<InputField>().text;

                break;
            case 4:
                panel4.SetActive(true);
                SaveloadSystem.ak41 = ak41.GetComponent<InputField>().text;
                SaveloadSystem.ak42 = ak42.GetComponent<InputField>().text;
                SaveloadSystem.ak43 = ak43.GetComponent<InputField>().text;
                SaveloadSystem.ak44 = ak44.GetComponent<InputField>().text;
                SaveloadSystem.ak45 = ak45.GetComponent<InputField>().text;
                break;
        }
        
        
        StartCoroutine(Upload());
    }

    IEnumerator Upload()
    {
        WWWForm form = new WWWForm();

        string user_id = SaveloadSystem.id;

        
        
        form.AddField("user_id", user_id);
        form.AddField("ak11", SaveloadSystem.ak11);
        form.AddField("ak12", SaveloadSystem.ak12);
        form.AddField("ak13", SaveloadSystem.ak13);
        form.AddField("ak14", SaveloadSystem.ak14);
        form.AddField("ak15", SaveloadSystem.ak15);
        form.AddField("ak21", SaveloadSystem.ak21);
        form.AddField("ak22", SaveloadSystem.ak22);
        form.AddField("ak23", SaveloadSystem.ak23);
        form.AddField("ak24", SaveloadSystem.ak24);
        form.AddField("ak25", SaveloadSystem.ak25);
        form.AddField("ak31", SaveloadSystem.ak31);
        form.AddField("ak32", SaveloadSystem.ak32);
        form.AddField("ak33", SaveloadSystem.ak33);
        form.AddField("ak34", SaveloadSystem.ak34);
        form.AddField("ak35", SaveloadSystem.ak35);
        form.AddField("ak41", SaveloadSystem.ak41);
        form.AddField("ak42", SaveloadSystem.ak42);
        form.AddField("ak43", SaveloadSystem.ak43);
        form.AddField("ak44", SaveloadSystem.ak44);
        form.AddField("ak45", SaveloadSystem.ak45);

        UnityWebRequest www = UnityWebRequest.Post("http://ahobaka.my.id/index.php/Welcome/setPostedText", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            timerCover = 1;
            textCover.GetComponent<Text>().text = www.error;
        }
        else
        {
            timerCover = 1;
            textCover.GetComponent<Text>().text = "Upload complete!";
            Debug.Log("Form upload complete!");
        }
    }
}
