﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class questionLoader : MonoBehaviour
{
    private GameObject saveLoadSystemGO;
    saveloadSystem SaveloadSystem;

    private GameObject coverLoadingGO;

    private GameObject qu1GO;
    private GameObject qu2GO;
    private GameObject imgGO;
    private GameObject img2GO;
    private GameObject img3GO;
    private GameObject aGO;
    private GameObject bGO;
    private GameObject cGO;
    private GameObject dGO;
    private GameObject eGO;
    private GameObject buttonNoSoal;
    private GameObject textCover;
    private GameObject scoreCover;
    private GameObject nextButton;

    private float timerCoverActivate;
    private float timerTrueActivate;

    private string urlDB = "http://ahobaka.my.id/index.php/Welcome/";
    
    private string idUser;
    private string namaUser;
    private string absenUser;
    private string nomorSoal;

    private string rightAnswer;
    string userAns;

    // Start is called before the first frame update
    void Start()
    {
        timerCoverActivate = 1;
        timerTrueActivate = 0;
        coverLoadingGO = GameObject.Find("coverLoading");
        saveLoadSystemGO = GameObject.Find("saveLoadSystem");
        SaveloadSystem = saveLoadSystemGO.GetComponent<saveloadSystem>();
        Debug.Log(SaveloadSystem.id);

        idUser = SaveloadSystem.id;
        namaUser = SaveloadSystem.nama;
        absenUser = SaveloadSystem.absen;
        nomorSoal = SaveloadSystem.eval;

        qu1GO = GameObject.Find("qu1");
        qu2GO = GameObject.Find("qu2");

        imgGO = GameObject.Find("img");
        img2GO = GameObject.Find("img2");
        img3GO = GameObject.Find("img3");
        nextButton = GameObject.Find("nextButton");

        aGO = GameObject.Find("a");
        bGO = GameObject.Find("b");
        cGO = GameObject.Find("c");
        dGO = GameObject.Find("d");
        eGO = GameObject.Find("e");

        buttonNoSoal = GameObject.Find("buttonNoSoal");
        buttonNoSoal.SetActive(false);

        textCover = GameObject.Find("textCover");
        scoreCover = GameObject.Find("scoreCover");

        scoreCover.SetActive(false);

        imgGO.SetActive(false);
        img2GO.SetActive(false);
        img3GO.SetActive(false);
        nextButton.SetActive(false);
        method_getSoal();
    }

    // Update is called once per frame
    void Update()
    {
        timerCoverActivate -= Time.deltaTime;
        timerTrueActivate -= Time.deltaTime;

        if (timerCoverActivate <= 0)
        {
            coverLoadingGO.SetActive(false);
        }
        else {
            coverLoadingGO.SetActive(true);
        }

        if (timerTrueActivate > 0 && timerTrueActivate <= 1) {
            SceneManager.LoadScene("evaluasi");
        }

        if (eGO.GetComponentInChildren<Toggle>().isOn == false) {
            Debug.Log("user sudah memilih");
            nextButton.SetActive(true);
        }
    }

    void method_getSoal()
    {
        StartCoroutine(numerator_GetSoal());
    }

    IEnumerator numerator_GetSoal()
    {
        UnityWebRequest www = UnityWebRequest.Get(urlDB + "getSoal/" + nomorSoal);
        yield return www.SendWebRequest();

        eGO.GetComponentInChildren<Toggle>().isOn = true;

        string result = www.downloadHandler.text;

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {

            if (result == "nosoal")
            {
                timerCoverActivate = 1000;
                scoreCover.SetActive(true);
                textCover.GetComponent<Text>().text = "Evaluasi Selesai...";
                scoreCover.GetComponent<Text>().text = "Score : "+ SaveloadSystem.score;

                SaveloadSystem.eval = 1.ToString();

                Debug.Log("nosoal");

                buttonNoSoal.SetActive(true);
            }
            else {

                string[] chunks = result.Split('~');
                
                qu1GO.GetComponent<Text>().text = chunks[1];
                qu2GO.GetComponent<Text>().text = chunks[3];

                aGO.GetComponent<Text>().text = chunks[4];
                bGO.GetComponent<Text>().text = chunks[5];
                cGO.GetComponent<Text>().text = chunks[6];
                dGO.GetComponent<Text>().text = chunks[7];
                rightAnswer = chunks[8];

                Debug.Log(chunks[2]);

                switch (chunks[2])
                {
                    case "1":
                        imgGO.SetActive(true);
                        break;
                    case "2":
                        img2GO.SetActive(true);
                        break;
                    case "3":
                        img3GO.SetActive(true);
                        break;
                    default:
                        imgGO.SetActive(false);
                        img2GO.SetActive(false);
                        img3GO.SetActive(false);
                        break;
                }

            }
              
        }
    }



    public void checkAns() {

        Debug.Log(SaveloadSystem.ans);

        userAns = null;

        if (aGO.GetComponentInChildren<Toggle>().isOn == true)
        {
            userAns = "a";
            Debug.Log("a is ceked");
        }
        else if (bGO.GetComponentInChildren<Toggle>().isOn == true)
        {
            userAns = "b";
            Debug.Log("b is ceked");
        }
        else if (cGO.GetComponentInChildren<Toggle>().isOn == true)
        {
            userAns = "c";
            Debug.Log("c is ceked");
        }
        else if (dGO.GetComponentInChildren<Toggle>().isOn == true)
        {
            userAns = "d";
            Debug.Log("d is ceked");
        }
        else
        {
            Debug.Log("no checed");
        }

        //StartCoroutine(saveAns());
        int nomor = int.Parse(SaveloadSystem.eval);
        SaveloadSystem.ans[nomor-1] = userAns;

        string ansString = string.Join(".", SaveloadSystem.ans); ;
        Debug.Log(ansString);

        if (userAns == rightAnswer)
        {
            Debug.Log("jawaban benar");
            int nomorSoalInt = int.Parse(nomorSoal) + 1;
            SaveloadSystem.eval = nomorSoalInt.ToString();
            timerCoverActivate = 1;

            SaveloadSystem.score = SaveloadSystem.score + 1;
            Debug.Log("score : "+ SaveloadSystem.score);



            //timerTrueActivate = 2;
          //  coverLoadingGO.GetComponentInChildren<Text>().text = "jawaban benar";
            //StartCoroutine(nextSoal());
        }
        else
        {
            Debug.Log("jawbaan salah");
            int nomorSoalInt = int.Parse(nomorSoal) + 1;
            SaveloadSystem.eval = nomorSoalInt.ToString();
            timerCoverActivate = 1;
            //StartCoroutine(nextSoal());
            //   coverLoadingGO.GetComponentInChildren<Text>().text = "jawaban Salah";
        }

        StartCoroutine(nextSoal());
    }


    public void resetSoal() {
        StartCoroutine(resetSoalC());
    }



    IEnumerator resetSoalC() {
        UnityWebRequest www = UnityWebRequest.Get(urlDB + "resetSoal/" + SaveloadSystem.id);
      //  Debug.Log(urlDB + "nextsoal/" + SaveloadSystem.id + "/" + SaveloadSystem.eval);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {

            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("reset soal berhasil");
            SceneManager.LoadScene("dashboard");
        }
    }

    IEnumerator saveAns()
    {

        string ansString = string.Join(".", SaveloadSystem.ans); ;
        Debug.Log(ansString);

        UnityWebRequest www = UnityWebRequest.Get(urlDB + "saveAns/" + SaveloadSystem.id + "/" + SaveloadSystem.ans);
        Debug.Log(urlDB + "saveAns/" + SaveloadSystem.id + "/" + SaveloadSystem.eval);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {

            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("save jawaban berhasil");

        }
    }

    IEnumerator nextSoal()
    {
        string ansString = string.Join(".", SaveloadSystem.ans); ;
        Debug.Log(ansString);

        
        UnityWebRequest www = UnityWebRequest.Get(urlDB + "nextsoal/" + SaveloadSystem.id + "/" + SaveloadSystem.eval+"/"+ansString+"/"+SaveloadSystem.score);
        Debug.Log(urlDB + "nextsoal/" + SaveloadSystem.id + "/" + SaveloadSystem.eval + "/" + ansString + "/" + SaveloadSystem.score);

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {

            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("next soal berhasil");
            SceneManager.LoadScene("evaluasi");

        }
    }
}
